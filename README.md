# ansible-role-zfs
Ansible role to create ZFS Pools

## Requirements
<span style="color:red">**On Debian you must have contrib repositories enabled**</span>

## How to install
### requirements.yml
**Put the file in your roles directory**
```yaml
---
- src: https://github.com/adieperi/ansible-role-zfs.git
  scm: git
  version: master
  name: ansible-role-zfs
```
### Download the role
```Shell
ansible-galaxy install -f -r ./roles/requirements.yml --roles-path=./roles
```
## Exemple Playbook
```yaml
---
- hosts: all
  tasks:

    - name: Destroy ZFS pool called "rpool"
      include_role:
        name: ansible-role-zfs
      vars:
        zpoolName: rpool
        zpoolState: absent

    - name: Create ZFS pool called "rpool"
      include_role:
        name: ansible-role-zfs
      vars:
        zpoolName: rpool
        zpoolState: present
        zpoolDevices:
          - /dev/sdb
          - /dev/sdc
          - /dev/sdd
        zpoolMode: raidz
        zpoolMountPoint: /mnt
        zpoolOptions:
          - ashift=12
          - atime=off
          - compression=gzip-9
          - normalization=formD
          - overlay=on

    - name: Create dataset called "home" in pool "rpool"
      include_role:
        name: ansible-role-zfs
      vars:
        zpoolName: rpool
        datasetName: home
        datasetState: present
        datasetOptions:
          - overlay=on
          - mountpoint="/home"
          - quota=5G
          - checksum=off

```
